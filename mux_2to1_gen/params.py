#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from inverter2_gen.params import inverter2_layout_params
from tgate_gen.params import tgate_layout_params


@dataclass
class mux_2to1_layout_params(LayoutParamsBase):
    """
    Parameter class for mux_2to1_gen

    Args:
    ----
    inverter_params : inverter2_params
        Parameters for inverter2 sub-generators

    tgate_params : tgate_params
        Parameters for tgate sub-generators

    show_pins : bool
        True to create pin labels
    """

    inverter_params: inverter2_layout_params
    tgate_params: tgate_layout_params
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> mux_2to1_layout_params:
        return mux_2to1_layout_params(
            inverter_params=inverter2_layout_params.finfet_defaults(min_lch),
            tgate_params=tgate_layout_params.finfet_defaults(min_lch),
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> mux_2to1_layout_params:
        return mux_2to1_layout_params(
            inverter_params=inverter2_layout_params.planar_defaults(min_lch),
            tgate_params=tgate_layout_params.planar_defaults(min_lch),
            show_pins=True,
        )


@dataclass
class mux_2to1_params(GeneratorParamsBase):
    layout_parameters: mux_2to1_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> mux_2to1_params:
        return mux_2to1_params(
            layout_parameters=mux_2to1_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
