.. tgate_gen documentation master file, created by
   sphinx-quickstart on Wed Feb  9 18:06:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tgate_gen's documentation!
===============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
.. automodule:: tgate_gen
   :members:
   :undoc-members:
.. automodule:: tgate_gen.layout
   :members:
   :undoc-members:
.. automodule:: tgate_gen.schematic
   :members:
   :undoc-members:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
