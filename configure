#!/bin/sh
MODULENAME=$(basename $(cd $(dirname ${0}) && pwd) ) 
if [[ $MODULENAME == *_gen ]]; then
    CELLNAME=${MODULENAME::-4}
else
    CELLNAME=$MODULENAME
fi

MODULELOCATION=$(cd `dirname ${0}`/.. && pwd )
TECHLIB="$(sed -n '/tech_lib/s/^.*tech_lib:\s*"//gp' \
    ${BAG_WORK_DIR}/bag_config.yaml | sed -n 's/".*$//p' )"

LVS="\${BAG_WORK_DIR}/shell/lvs.sh"

PEX="\${BAG_WORK_DIR}/shell/pex.sh"

if [ -f "${MODULELOCATION}/${MODULENAME}/lvs_box.txt" ]; then
    LVSBOXSTRING="-C ${MODULELOCATION}/${MODULENAME}/lvs_box.txt"
else
    LVSBOXSTRING=""
fi


LVSOPTS="\
    -c ${CELLNAME} \
    ${LVSBOXSTRING} \
    -f \
    -G \"VSS\" \
    -l ${CELLNAME}_generated \
    -v \"VDD VSS\" \
    -S \"VDD\" \
    -t ${TECHLIB} \
"
PEXOPTS="\
    -c ${CELLNAME} \
    ${LVSBOXSTRING} \
    -f \
    -G \"VSS\" \
    -l ${CELLNAME}_generated \
    -R \"0.1 0.01 0.1 0.01\"  \
    -v \"VDD VSS\" \
    -S \"VDD\" \
    -t ${TECHLIB} \
"

DRC="\${BAG_WORK_DIR}/shell/drc.sh" 
DRCOPTS="\
    -c ${CELLNAME} \
    -d \
    -f \
    -l ${CELLNAME}_drc_run \
    -L \
    -g ${BAG_WORK_DIR}/${CELLNAME}_lvs_run/${CELLNAME}.calibre.db \
"

for purpose in templates; do
    if [ -z "$(grep ${CELLNAME}_${purpose} ${MODULELOCATION}/cds.lib)" ]; then
        echo "Adding ${CELLNAME}_${purpose} to $MODULELOCATION/cds.lib"
        echo "DEFINE  ${CELLNAME}_${purpose} \${BAG_WORK_DIR}/${MODULENAME}/${CELLNAME}_${purpose}" >> ${MODULELOCATION}/cds.lib
    fi
done

CURRENTFILE="${MODULELOCATION}/${MODULENAME}/Makefile"
echo "Generating ${CURRENTFILE}"
cat << EOF > ${CURRENTFILE}
LVS = ${LVS//    }
LVSOPTS =  ${LVSOPTS//    }
PEX = ${PEX//    }
PEXOPTS =  ${PEXOPTS//    }
DRC = ${DRC//    }
DRCOPTS =  ${DRCOPTS//    }

MODULELOCATION = ${MODULELOCATION}
MODULENAME = ${MODULENAME}
CELLNAME = ${CELLNAME}

DEP0 := \${BAG_WORK_DIR}/BagModules/\$(CELLNAME)_templates/netlist_info/\$(CELLNAME).yaml

BUILDOPTS ?= --finfet

# Runs that are used in mutiple places
define gen-run =
cd \${BAG_WORK_DIR} && \\
\${BAG_PYTHON} \$(MODULELOCATION)/\$(MODULENAME)/main.py
endef

define lvs-run =
cd \${BAG_WORK_DIR} && \\
\$(LVS) \$(LVSOPTS)
endef

define pex-run =
cd \${BAG_WORK_DIR} && \\
\$(PEX) \$(PEXOPTS)
endef

.PHONY: all doc gen flip_well finfet lvs drc pex clean

# gen twice for initial mapping
all: doc gen lvs drc pex

# Yaml file is generated with very first run that requires re-execution
# Therefore the dependency
doc:
	cd \${BAG_WORK_DIR}/\$(MODULENAME)/doc && make html

gen:  \$(DEP0) 
	\$(gen-run) \$(BUILDOPTS)

flip_well:  \$(DEP0) 
	\$(gen-run) --flip_well

finfet:  \$(DEP0) 
	\$(gen-run) --finfet

cmos:  \$(DEP0) 
	\$(gen-run)

lvs: \${BAG_WORK_DIR}/\$(CELLNAME)_generated/\$(CELLNAME)/layout/layout.oa
	\$(lvs-run)

pex: \${BAG_WORK_DIR}/\$(CELLNAME)_generated/\$(CELLNAME)/layout/layout.oa
	\$(pex-run)

drc: \${BAG_WORK_DIR}/\$(CELLNAME)_lvs_run/\$(CELLNAME).calibre.db
	cd \${BAG_WORK_DIR} && \\
    \$(DRC) \$(DRCOPTS)

# Ensure re-generation if dependency missing
\$(DEP0):
	cd \${BAG_WORK_DIR} && \${BAG_PYTHON} \${BAG_WORK_DIR}/\$(MODULENAME)/main.py

\${BAG_WORK_DIR}/\$(CELLNAME)_generated/\$(CELLNAME)/layout/layout.oa:
	\$(gen-run)

\${BAG_WORK_DIR}/\$(CELLNAME)_lvs_run/\$(CELLNAME).calibre.db: \${BAG_WORK_DIR}/\$(CELLNAME)_generated/\$(CELLNAME)/layout/layout.oa
	\$(lvs-run)

clean: 
	sed -i "/\$(CELLNAME)_templates/d" \${BAG_WORK_DIR}/bag_libs.def
	rm -rf  \${BAG_WORK_DIR}/BagModules/\$(CELLNAME)_templates
	rm -rf  \${BAG_WORK_DIR}/\$(CELLNAME)_lvs_run
	rm -rf  \${BAG_WORK_DIR}/\$(CELLNAME)_drc_run

EOF

exit 0

