#! /usr/bin/env python3

from mux_2to1_gen.design import design


if __name__ == '__main__':
    """
    NOTE: This method is only used when using configure/make to run the generator (deprecated).

    In constrast, the 'gen' script always directly imports the generator Python package
    and instantiates the design class found in this design.py module.

    See gen --help for information about the generator script.
    The script resides in BAG_methods/sal/generator.py
    """
    inst = design()
    inst.generate()
